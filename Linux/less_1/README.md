# Linux - Less_1

## Practice - Lesson1_Linux Introduction & Lesson1_Basic command line skills
1. Vagrantfile:   
        Vagrant.configure("2") do |config|  
            config.vm.box = "bento/ubuntu-20.04"  
        end  
    vagrant up  
    vagrant ssh  

    vagrant@vagrant:/vagrant$  

2.  mkdir lesson1  
   
3.  mkdir dir1 dir2    
    touch dir1/f1 dir1/f2    
   
4.  cp -r dir1 dir2  

5.  rm -r dir1  

6.  cp /etc/services ~/lesson1/  

7.  cat services | grep HTTP > ~/lesson1/dir2/dir1/f1  

8.  wc -l  ~/lesson1/dir2/dir1/f1 > ~/lesson1/dir2/dir1/f2  

9.   ncal -o 2051  
     05/07/2051  

Tests:
    cd /vagrant  
    chmod +x Lesson1_Basic_command_line_skills.sh  
    root@vagrant:/vagrant# ./Lesson1_Basic_command_line_skills.sh -123456  
    TEST 1: PASS  
    TEST 2: ERROR - no such dir1; task 5. Remove dir1  
    TEST 3: PASS  
    TEST 4: PASS  
    TEST 5: PASS  
    TEST 6: PASS  

## Practice - Lesson1_SSH  
1.  To server:  
    useradd -m mysshfriend  
    usermod -a -G sudo mysshfriend  
    passwd mysshfriend  
    nano /etc/ssh/sshd_config  
    uncomment:  
        Port 22  
        ListenAddress 192.168.56.101  
        PubkeyAutentikation yes  
        AuthorizedKeysFile .ssh/authorized_keys  
        PasswordAuthentication yes  
2.  To client:  
    cd ~/.ssh  
    ssh-keygen -t rsa  
    cd ..   
    ssh-copy-id -i .ssh/ubuntu-test.pub mysshfriend@192.168.56.101    
    /usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: ".ssh/ubuntu-test.pub"    
    /usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed    
    /usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys  
    mysshfriend@192.168.56.101's password:     

    Number of key(s) added: 1    

    Now try logging into the machine, with:   "ssh 'mysshfriend@192.168.56.101'"    
    and check to make sure that only the key(s) you wanted were added.    

    ssh mysshfriend@192.168.56.101  
    Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.11.0-41-generic x86_64)  

    * Documentation:  https://help.ubuntu.com  
    * Management:     https://landscape.canonical.com  
    * Support:        https://ubuntu.com/advantage  

    130 updates can be applied immediately.  
    60 of these updates are standard security updates.  
    To see these additional updates run: apt list --upgradable  

    Failed to connect to https://changelogs.ubuntu.com/meta-release-lts. Check your Internet connection or proxy settings  

    Your Hardware Enablement Stack (HWE) is supported until April 2025.  
    Last login: Mon Dec  6 02:20:27 2021 from 192.168.56.99  
    $ exit  
    Connection to 192.168.56.101 closed.  








