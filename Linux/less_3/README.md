# Linux - Less_3
## Practice - Lesson3_Basic_monitoring - Part_1
1.  vagrant@vagrant:~$ sleep 133337 &  
    [1] 12623  
    vagrant@vagrant:~$ ps aux | grep sleep  
    vagrant    12623  0.0  0.0   8076   592 pts/0    S    18:38   0:00 sleep 133337  
    vagrant    12627  0.0  0.0   8900   728 pts/0    S+   18:39   0:00 grep --color=auto sleep  
    vagrant@vagrant:~$ jobs  
    [1]+  Running                 sleep 133337 &  

2.  vagrant@vagrant:~$ fg 1  
    sleep 133337  

3.  vagrant@vagrant:~$ ping ya.ru > /dev/null &  
    [2] 12740  
    vagrant@vagrant:~$ jobs  
    [1]+  Stopped                 sleep 133337  
    [2]-  Running                 ping ya.ru > /dev/null &  
    vagrant@vagrant:~$ ps aux | grep ping  
    vagrant    12740  0.0  0.2   9808  2676 pts/0    S    19:11   0:00 ping ya.ru  
    vagrant    12742  0.0  0.0   8900   672 pts/0    S+   19:11   0:00 grep --color=auto ping  

4.  vagrant@vagrant:~$ sudo dmidecode -s system-product-name  
    VirtualBox  
    vagrant@vagrant:~$ lscpu > lscpu.txt && cat lscpu.txt | grep Hypervisor  
    Hypervisor vendor:               KVM  
    vagrant@vagrant:~$ lspci | grep System  
    00:02.0 VGA compatible controller: InnoTek Systemberatung GmbH VirtualBox Graphics Adapter  
    00:04.0 System peripheral: InnoTek Systemberatung GmbH VirtualBox Guest Service  

5.  top | grep wa  
    %Cpu(s):  3.1 us,  0.0 sy,  0.0 ni, 96.9 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st  
    wa: io wait cpu time (or) % CPU time spent in wait on I/O  

    vagrant@vagrant:~$ iotop  



## Practice - Lesson3_Basic_monitoring - Part_2
1.  vagrant@vagrant:~$ uptime  
    19:52:19 up  1:15,  1 user,  load average: 0.00, 0.00, 0.00  
    vagrant@vagrant:~$ nproc  
    2  

    LA <= 2

2.  vagrant@vagrant:~$ stress --cpu 1 --io 1 --vm-bytes 500M  
    stress: info: [13378] dispatching hogs: 1 cpu, 1 io, 0 vm, 0 hdd  

    vagrant@vagrant:~$ top  
    top - 20:11:04 up  1:34,  2 users,  load average: 0.91, 0.43, 0.21  
    Tasks: 111 total,   2 running, 104 sleeping,   5 stopped,   0 zombie  
    %Cpu0  :  0.0 us,  0.5 sy,  0.0 ni,  0.0 id, 93.4 wa,  0.0 hi,  6.2 si,  0.0 st  
    %Cpu1  :100.0 us,  0.0 sy,  0.0 ni,  0.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st  
    MiB Mem :    981.0 total,    397.5 free,     98.4 used,    485.1 buff/cache  
    MiB Swap:    980.0 total,    980.0 free,      0.0 used.    730.3 avail Mem  

3.  vagrant@vagrant:~$ ps -FA --sort pcpu | tail -n 3  
    vagrant    13379   13378  2   964   104   1 20:10 pts/0    00:01:00 stress --cpu 1 --io 1 --vm-bytes 500M  
    vagrant    13385   13384  4   964   100   1 20:13 pts/0    00:01:40 stress --cpu 1 --io 1 --vm-bytes 500M --hdd-bytes 32G  
    vagrant    13356   13355  6   964   104   0 20:04 pts/0    00:02:41 stress --cpu 1 

    vagrant@vagrant:~$ ps -FA --sort pmem | tail -n 3  
    root         362       1  0 13327 15748   1 18:36 ?        00:00:00 /lib/systemd/systemd-journald  
    root         609       1  0  7909 17904   1 18:36 ?        00:00:00 /usr/bin/python3 /usr/bin/networkd-dispatcher  --run-startup-triggers  
    root         524       1  0 70050 17992   1 18:36 ?        00:00:01 /sbin/multipathd -d -s  

4.  vagrant@vagrant:~$ pstree  
    systemd─┬─VBoxService───8*[{VBoxService}]  
            ├─accounts-daemon───2*[{accounts-daemon}]  
            ├─agetty  
            ├─atd  
            ├─cron  
            ├─dbus-daemon  
            ├─irqbalance───{irqbalance}  
            ├─multipathd───6*[{multipathd}]  
            ├─networkd-dispat  
            ├─polkitd───2*[{polkitd}]  
            ├─rpcbind  
            ├─rsyslogd───3*[{rsyslogd}]  
            ├─sshd─┬─sshd───sshd───bash─┬─grep  
            │      │                    ├─pstree  
            │      │                    ├─sleep  
            │      │                    ├─stress───stress  
            │      │                    ├─2*[stress───2*[stress]]  
            │      │                    └─top  
            │      └─sshd───sshd───bash  
            ├─systemd───(sd-pam)  
            ├─systemd-journal  
            ├─systemd-logind  
            ├─systemd-network  
            ├─systemd-resolve  
            └─systemd-udevd  

5.  vagrant@vagrant:~$ kill -9 `lsof -t -u vagrant`  
    Connection to 127.0.0.1 closed.  


 



 



  
