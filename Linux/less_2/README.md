# Linux - Less_2  
## Practice - Lesson2_Variables  
1.  vagrant@test:~$ L2V="hello"  
    vagrant@test:~$ $L2V  
    -bash: hello: command not found  

2.  vagrant@test:~$ which vi  
    /usr/bin/vi  

3.  vagrant@test:~$ env | grep USER    
    USER=vagrant   

    vagrant@test:~$ $USER  
    -bash: vagrant: command not found  

4.  vagrant@test:~$ $LAB  
    -bash: less_2: command not found  
    vagrant@test:~$ env | grep LAB  
    LAB=less_2  

## Practice - Lesson2_Users_Groups_and_Superusers
Vagrantile:  
    Vagrant.configure("2") do |config|  
    config.vm.define "test" do |t|  
       t.vm.box = "centos/7"  
       t.vm.hostname = "test"  
      end   
end  

1.  [vagrant@test ~]$ su  
    Password:   
    [root@test vagrant]# whoami  
    root  

2.  [root@test vagrant]# passwd   
    Changing password for user root.  
    New password:   
    BAD PASSWORD: The password contains the user name in some form  
    Retype new password:   
    passwd: all authentication tokens updated successfully.  

3.  [vagrant@test ~]$ sudo su  
    [root@test vagrant]# whoami  
    root  


4.  [root@test vagrant]# cd /etc/skel  
    [root@test skel]# touch welcome.txt  
    [root@test skel]# nano welcome.txt  

5.  [root@test vagrant]# sudo useradd newuser -m -d /home/newuser -s /bin/bash  
    [root@test vagrant]# sudo passwd newuser  
    Changing password for user newuser.  
    New password:   
    BAD PASSWORD: The password is shorter than 8 characters  
    Retype new password:   
    passwd: all authentication tokens updated successfully.  
    [root@test vagrant]# su newuser  
    [newuser@test vagrant]$ cd ~  
    [newuser@test ~]$ ls -al  
    total 12  
    drwx------. 2 newuser newuser  81 Dec  8 20:15 .  
    drwxr-xr-x. 4 root    root     36 Dec  8 20:15 ..  
    -rw-r--r--. 1 newuser newuser  18 Apr  1  2020 .bash_logout  
    -rw-r--r--. 1 newuser newuser 193 Apr  1  2020 .bash_profile  
    -rw-r--r--. 1 newuser newuser 231 Apr  1  2020 .bashrc  
    -rw-r--r--. 1 newuser newuser   0 Dec  8 20:08 welcome.txt  

6.  [root@test ~]# visudo  

    root    ALL=(ALL)       ALL  
    newuser ALL=(ALL)       ALL    

    [newuser@test vagrant]$ sudo su
    [sudo] password for newuser: 
    [root@test vagrant]# 

## Practice - Lesson2_Linux_FS_and_Perpissions  
1.  Vagrant.configure("2") do |config|  
        config.vm.box = "bento/ubuntu-20.04"  
        config.vm.provider :virtualbox do |vb|  
            lvm_experiments_disk0_path = "/tmp/lvm_experiments_disk0.vmdk"  
            vb.customize ['createmedium', '--filename', lvm_experiments_disk0_path, '--size', 2560]  
            vb.customize ['storageattach', :id, '--storagectl', 'SATA Controller', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', lvm_experiments_disk0_path]  
            end  
        end  

2.  vagrant@vagrant:~$ lsblk  
    NAME                 MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT  
    sda                    8:0    0   64G  0 disk   
    ├─sda1                 8:1    0  512M  0 part /boot/efi  
    ├─sda2                 8:2    0    1K  0 part   
    └─sda5                 8:5    0 63.5G  0 part   
    ├─vgvagrant-root   253:0    0 62.6G  0 lvm  /  
    └─vgvagrant-swap_1 253:1    0  980M  0 lvm  [SWAP]  
    sdb                    8:16   0  2.5G  0 disk   

    vagrant@vagrant:~$ fdisk /dev/sdb  
    Command (m for help): n  
    Partition type  
    p   primary (0 primary, 0 extended, 4 free)  
    e   extended (container for logical partitions)  
    Select (default p): p  
    Partition number (1-4, default 1): 1  
    First sector (2048-5242879, default 2048):   
    Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-5242879, default 5242879): +1G  

    Created a new partition 1 of type 'Linux' and of size 1 GiB.  

    Command (m for help): n  
    Partition type  
    p   primary (1 primary, 0 extended, 3 free)  
    e   extended (container for logical partitions)  
    Select (default p): p  
    Partition number (2-4, default 2): 2  
    First sector (2099200-5242879, default 2099200):   
    Last sector, +/-sectors or +/-size{K,M,G,T,P} (2099200-5242879, default 5242879): +1400M  

    Created a new partition 2 of type 'Linux' and of size 1.4 GiB.  

    Command (m for help): w  
    The partition table has been altered.  
    Calling ioctl() to re-read partition table.  
    Syncing disks.  

    vagrant@vagrant:~$ lsblk  
    NAME                 MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT  
    sda                    8:0    0   64G  0 disk   
    ├─sda1                 8:1    0  512M  0 part /boot/efi  
    ├─sda2                 8:2    0    1K  0 part   
    └─sda5                 8:5    0 63.5G  0 part   
    ├─vgvagrant-root   253:0    0 62.6G  0 lvm  /  
    └─vgvagrant-swap_1 253:1    0  980M  0 lvm  [SWAP]  
    sdb                    8:16   0  2.5G  0 disk   
    ├─sdb1                 8:17   0    1G  0 part   
    └─sdb2                 8:18   0  1.4G  0 part   

    vagrant@vagrant:~$ sudo mkfs.ext3 /dev/sdb1  
    mke2fs 1.45.5 (07-Jan-2020)  
    Creating filesystem with 262144 4k blocks and 65536 inodes  
    Filesystem UUID: 2bce55f6-ea2c-483e-a4b5-fa417cbf5ad9  
    Superblock backups stored on blocks:   
        32768, 98304, 163840, 229376  

    Allocating group tables: done                              
    Writing inode tables: done                              
    Creating journal (8192 blocks): done  
    Writing superblocks and filesystem accounting information: done  

    vagrant@vagrant:~$ sudo mkfs.btrfs /dev/sdb2  
    btrfs-progs v5.4.1   
    See http://btrfs.wiki.kernel.org for more information.  

    Label:              (null)  
    UUID:               c69be9f4-2f93-4939-9039-1adf46108410  
    Node size:          16384  
    Sector size:        4096  
    Filesystem size:    1.37GiB  
    Block group profiles:  
    Data:             single            8.00MiB  
    Metadata:         DUP              70.00MiB  
    System:           DUP               8.00MiB  
    SSD detected:       no  
    Incompat features:  extref, skinny-metadata  
    Checksum:           crc32c  
    Number of devices:  1  
    Devices:  
    ID        SIZE  PATH  
        1     1.37GiB  /dev/sdb2  

    root@vagrant:/# mkdir -p /homework/ext /homework/btrfs  
    root@vagrant:/# ls /homework  
    btrfs  ext  

    root@vagrant:/# mount /dev/sdb1 /homework/ext  
    root@vagrant:/# mount /dev/sdb2 /homework/btrfs  

    root@vagrant:/# lsblk  
    NAME                 MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT  
    sda                    8:0    0   64G  0 disk   
    ├─sda1                 8:1    0  512M  0 part /boot/efi  
    ├─sda2                 8:2    0    1K  0 part   
    └─sda5                 8:5    0 63.5G  0 part   
    ├─vgvagrant-root   253:0    0 62.6G  0 lvm  /  
    └─vgvagrant-swap_1 253:1    0  980M  0 lvm  [SWAP]  
    sdb                    8:16   0  2.5G  0 disk   
    ├─sdb1                 8:17   0    1G  0 part /homework/ext  
    └─sdb2                 8:18   0  1.4G  0 part /homework/btrfs  

    root@vagrant:/# blkid /dev/sdb1  
    /dev/sdb1: UUID="2bce55f6-ea2c-483e-a4b5-fa417cbf5ad9" TYPE="ext3" PARTUUID="fe0af875-01"  
    root@vagrant:/# blkid /dev/sdb2  
    /dev/sdb2: UUID="c69be9f4-2f93-4939-9039-1adf46108410" UUID_SUB="c9f3fb90-5e18-4c6f-9039-d15fa34d43ca" TYPE="btrfs" PARTUUID="fe0af875-02"  

    root@vagrant:/# lsblk -o +uuid,name  
    NAME                 MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT      UUID                                   NAME  
    sda                    8:0    0   64G  0 disk                                                        sda  
    ├─sda1                 8:1    0  512M  0 part /boot/efi       7D3B-6BE4                              sda1  
    ├─sda2                 8:2    0    1K  0 part                                                        sda2  
    └─sda5                 8:5    0 63.5G  0 part                 Mx3LcA-uMnN-h9yB-gC2w-qm7w-skx0-OsTz9z sda5  
    ├─vgvagrant-root   253:0    0 62.6G  0 lvm  /               b527b79c-7f45-4e2b-a90f-1a4e9cb477c2   vgvagrant-root  
    └─vgvagrant-swap_1 253:1    0  980M  0 lvm  [SWAP]          fad91b1f-6eed-4e4b-8dbf-913ba5bcacc7   vgvagrant-swap_1  
    sdb                    8:16   0  2.5G  0 disk                                                        sdb  
    ├─sdb1                 8:17   0    1G  0 part /homework/ext   2bce55f6-ea2c-483e-a4b5-fa417cbf5ad9   sdb1  
    └─sdb2                 8:18   0  1.4G  0 part /homework/btrfs c69be9f4-2f93-4939-9039-1adf46108410   sdb2  

    root@vagrant:/# vi /etc/fstab  
    /dev/mapper/vgvagrant-root /               ext4    errors=remount-ro 0       1  
    UUID=7D3B-6BE4  /boot/efi       vfat    umask=0077      0       1  
    /dev/mapper/vgvagrant-swap_1 none            swap    sw              0       0  
    /dev/sdb1       /homework/ext   ext3  
    /dev/sdb2       /homework/btrfs btrfs  

    root@vagrant:/# reboot  
    Connection to 127.0.0.1 closed by remote host.  
    Connection to 127.0.0.1 closed.  
    k0pec@k0pec-ub:~/epam/epam_devops$ vagrant ssh  
    Welcome to Ubuntu 20.04.2 LTS (GNU/Linux 5.4.0-80-generic x86_64)  

    * Documentation:  https://help.ubuntu.com  
    * Management:     https://landscape.canonical.com  
    * Support:        https://ubuntu.com/advantage  

    System information as of Wed 08 Dec 2021 09:27:34 PM UTC  

    System load:  0.0               Processes:             142  
    Usage of /:   2.4% of 61.31GB   Users logged in:       0  
    Memory usage: 14%               IPv4 address for eth0: 10.0.2.15  
    Swap usage:   0%  

    vagrant@vagrant:~$ lsblk  
    NAME                 MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT  
    sda                    8:0    0   64G  0 disk   
    ├─sda1                 8:1    0  512M  0 part /boot/efi  
    ├─sda2                 8:2    0    1K  0 part   
    └─sda5                 8:5    0 63.5G  0 part   
    ├─vgvagrant-root   253:0    0 62.6G  0 lvm  /  
    └─vgvagrant-swap_1 253:1    0  980M  0 lvm  [SWAP]  
    sdb                    8:16   0  2.5G  0 disk   
    ├─sdb1                 8:17   0    1G  0 part /homework/ext  
    └─sdb2                 8:18   0  1.4G  0 part /homework/btrfs  












