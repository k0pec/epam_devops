# Linux - Less_4
## Practice - Lesson4_SystemD
1.  ```
    k0pec@k0pec-ub:~/epam/epam_devops/Linux$ journalctl | head  
    -- Logs begin at Thu 2021-10-28 23:27:11 +03, end at Sun 2021-12-12 23:55:01 +03. --  
    окт 28 23:27:11 k0pec-ub kernel: microcode: microcode updated early to revision 0xea, date = 2021-01-05  
    окт 28 23:27:11 k0pec-ub kernel: Linux version 5.11.0-38-generic     (buildd@lgw01-amd64-041) (gcc (Ubuntu 9.3.0-17ubuntu1~20.04) 9.3.0, GNU ld (GNU Binutils for Ubuntu) 2.34) #42~20.04.  1-Ubuntu SMP Tue Sep 28 20:41:07 UTC 2021 (Ubuntu 5.11.0-38.42~20.04.1-generic 5.11.22)  
    окт 28 23:27:11 k0pec-ub kernel: Command line: BOOT_IMAGE=/boot/vmlinuz-5.11.0-38-generic   root=UUID=e620270f-a453-472c-a6d5-53863f6e9cd8 ro quiet splash vt.handoff=7  
    окт 28 23:27:11 k0pec-ub kernel: KERNEL supported cpus:  
    окт 28 23:27:11 k0pec-ub kernel:   Intel GenuineIntel  
    окт 28 23:27:11 k0pec-ub kernel:   AMD AuthenticAMD  
    окт 28 23:27:11 k0pec-ub kernel:   Hygon HygonGenuine  
    окт 28 23:27:11 k0pec-ub kernel:   Centaur CentaurHauls  
    окт 28 23:27:11 k0pec-ub kernel:   zhaoxin   Shanghai    
    ```
2.  ```
    k0pec@k0pec-ub:~/epam/epam_devops/Linux$ journalctl --boot | head
    -- Logs begin at Thu 2021-10-28 23:27:11 +03, end at Mon 2021-12-13 00:00:44 +03. --
    дек 12 21:31:04 k0pec-ub kernel: microcode: microcode updated early to revision 0xea, date = 2021-01-05
    дек 12 21:31:04 k0pec-ub kernel: Linux version 5.11.0-41-generic (buildd@lgw01-amd64-005) (gcc (Ubuntu 9.3.0-17ubuntu1~20.04) 9.3.0, GNU ld (GNU Binutils for Ubuntu) 2.34) #45~20.04.1-Ubuntu SMP Wed Nov 10 10:20:10 UTC 2021 (Ubuntu 5.11.0-41.45~20.04.1-generic 5.11.22)
    дек 12 21:31:04 k0pec-ub kernel: Command line: BOOT_IMAGE=/boot/vmlinuz-5.11.0-41-generic root=UUID=e620270f-a453-472c-a6d5-53863f6e9cd8 ro quiet splash vt.handoff=7
    дек 12 21:31:04 k0pec-ub kernel: KERNEL supported cpus:
    дек 12 21:31:04 k0pec-ub kernel:   Intel GenuineIntel
    дек 12 21:31:04 k0pec-ub kernel:   AMD AuthenticAMD
    дек 12 21:31:04 k0pec-ub kernel:   Hygon HygonGenuine
    дек 12 21:31:04 k0pec-ub kernel:   Centaur CentaurHauls
    дек 12 21:31:04 k0pec-ub kernel:   zhaoxin   Shanghai  
    ```
3.  ```
    k0pec@k0pec-ub:~/epam/epam_devops/Linux$ journalctl --until=yesterday | tail
    дек 11 23:54:00 k0pec-ub gnome-shell[2387]: Window manager warning: Overwriting existing binding of keysym 39 with keysym 39 (keycode 12).
    дек 11 23:55:01 k0pec-ub CRON[7546]: pam_unix(cron:session): session opened for user root by (uid=0)
    дек 11 23:55:01 k0pec-ub CRON[7547]: (root) CMD (command -v debian-sa1 > /dev/null && debian-sa1 1 1)
    дек 11 23:55:01 k0pec-ub CRON[7546]: pam_unix(cron:session): session closed for user root
    дек 11 23:56:46 k0pec-ub systemd[2124]: Started Application launched by gnome-shell.
    дек 11 23:56:46 k0pec-ub systemd[2124]: Started snap.code.code.be48dd74-a304-4678-b9b1-c42d9fab5a11.scope.
    дек 11 23:56:46 k0pec-ub systemd[2124]: gnome-launched-code_code.desktop-7558.scope: Succeeded.
    дек 11 23:59:01 k0pec-ub CRON[7793]: pam_unix(cron:session): session opened for user root by (uid=0)
    дек 11 23:59:01 k0pec-ub CRON[7794]: (root) CMD (command -v debian-sa1 > /dev/null && debian-sa1 60 2)
    дек 11 23:59:02 k0pec-ub CRON[7793]: pam_unix(cron:session): session closed for user root

    ```
    ```
    k0pec@k0pec-ub:~/epam/epam_devops/Linux$ journalctl --until=yesterday | tail
    дек 11 23:54:00 k0pec-ub gnome-shell[2387]: Window manager warning: Overwriting existing binding of keysym 39 with keysym 39 (keycode 12).
    дек 11 23:55:01 k0pec-ub CRON[7546]: pam_unix(cron:session): session opened for user root by (uid=0)
    дек 11 23:55:01 k0pec-ub CRON[7547]: (root) CMD (command -v debian-sa1 > /dev/null && debian-sa1 1 1)
    дек 11 23:55:01 k0pec-ub CRON[7546]: pam_unix(cron:session): session closed for user root
    дек 11 23:56:46 k0pec-ub systemd[2124]: Started Application launched by gnome-shell.
    дек 11 23:56:46 k0pec-ub systemd[2124]: Started snap.code.code.be48dd74-a304-4678-b9b1-c42d9fab5a11.scope.
    дек 11 23:56:46 k0pec-ub systemd[2124]: gnome-launched-code_code.desktop-7558.scope: Succeeded.
    дек 11 23:59:01 k0pec-ub CRON[7793]: pam_unix(cron:session): session opened for user root by (uid=0)
    дек 11 23:59:01 k0pec-ub CRON[7794]: (root) CMD (command -v debian-sa1 > /dev/null && debian-sa1 60 2)
    дек 11 23:59:02 k0pec-ub CRON[7793]: pam_unix(cron:session): session closed for user root
    ```
4.  ```
    k0pec@k0pec-ub:~/epam/epam_devops/Linux$ journalctl --unit=sshd
    -- Logs begin at Thu 2021-10-28 23:27:11 +03, end at Mon 2021-12-13 00:05:01 +03. --
    -- No entries --
    ```
5.  ```
    k0pec@k0pec-ub:~/epam/epam_devops/Linux$ journalctl -f
    -- Logs begin at Thu 2021-10-28 23:27:11 +03. --
    дек 13 00:00:34 k0pec-ub kernel: audit: type=1400 audit(1639342834.641:73): apparmor="DENIED" operation="capable" profile="/usr/sbin/cups-browsed" pid=9875 comm="cups-browsed" capability=23  capname="sys_nice"
    дек 13 00:00:35 k0pec-ub systemd[1]: man-db.service: Succeeded.
    дек 13 00:00:35 k0pec-ub systemd[1]: Finished Daily man-db regeneration.
    дек 13 00:00:44 k0pec-ub fstrim[9846]: /boot/efi: 511 MiB (535785472 bytes) trimmed on /dev/sda1
    дек 13 00:00:44 k0pec-ub fstrim[9846]: /: 184,1 GiB (197701394432 bytes) trimmed on /dev/sda5
    дек 13 00:00:44 k0pec-ub systemd[1]: fstrim.service: Succeeded.
    дек 13 00:00:44 k0pec-ub systemd[1]: Finished Discard unused blocks on filesystems from /etc/fstab.
    дек 13 00:05:01 k0pec-ub CRON[9979]: pam_unix(cron:session): session opened for user root by (uid=0)
    дек 13 00:05:01 k0pec-ub CRON[9980]: (root) CMD (command -v debian-sa1 > /dev/null && debian-sa1 1 1)
    дек 13 00:05:01 k0pec-ub CRON[9979]: pam_unix(cron:session): session closed for user root
    ```
6.  ```
    k0pec@k0pec-ub:~/epam/epam_devops/Linux$ journalctl -n 100 |wc -l
    101
    ```
    ```
    k0pec@k0pec-ub:~/epam/epam_devops/Linux$ journalctl -n 100 | tail
    дек 13 00:00:34 k0pec-ub kernel: audit: type=1400 audit(1639342834.641:73): apparmor="DENIED" operation="capable" profile="/usr/sbin/cups-browsed" pid=9875 comm="cups-browsed" capability=23  capname="sys_nice"
    дек 13 00:00:35 k0pec-ub systemd[1]: man-db.service: Succeeded.
    дек 13 00:00:35 k0pec-ub systemd[1]: Finished Daily man-db regeneration.
    дек 13 00:00:44 k0pec-ub fstrim[9846]: /boot/efi: 511 MiB (535785472 bytes) trimmed on /dev/sda1
    дек 13 00:00:44 k0pec-ub fstrim[9846]: /: 184,1 GiB (197701394432 bytes) trimmed on /dev/sda5
    дек 13 00:00:44 k0pec-ub systemd[1]: fstrim.service: Succeeded.
    дек 13 00:00:44 k0pec-ub systemd[1]: Finished Discard unused blocks on filesystems from /etc/fstab.
    дек 13 00:05:01 k0pec-ub CRON[9979]: pam_unix(cron:session): session opened for user root by (uid=0)
    дек 13 00:05:01 k0pec-ub CRON[9980]: (root) CMD (command -v debian-sa1 > /dev/null && debian-sa1 1 1)
    дек 13 00:05:01 k0pec-ub CRON[9979]: pam_unix(cron:session): session closed for user root
    ```
## Practice - Lesson4_PackageManagement
1.  ```
    vagrant@vagrant:~$ sudo apt edit-sources
    ```
    ```
    /etc/apt/sources.list 
    ```
2.  ```
    vagrant@vagrant:~$ echo "deb [signed-by=/usr/share/keyrings/nginx-archive-keyring.gpg] \
    > http://nginx.org/packages/ubuntu `lsb_release -cs` nginx" \
    >     | sudo tee /etc/apt/sources.list.d/nginx.list
    deb [signed-by=/usr/share/keyrings/nginx-archive-keyring.gpg] http://nginx.org/packages/ubuntu focal nginx
    ```
    ```
    vagrant@vagrant:~$ echo -e "Package: *\nPin: origin nginx.org\nPin: release o=nginx\nPin-Priority: 900\n" \
    >     | sudo tee /etc/apt/preferences.d/99nginx
    Package: *
    Pin: origin nginx.org
    Pin: release o=nginx
    Pin-Priority: 900
    ```
    ```
    sudo apt update
    sudo apt install nginx
    ```
    ```
    vagrant@vagrant:~$ sudo apt list | grep nginx | head

    WARNING: apt does not have a stable CLI interface. Use with caution in scripts.

    kopano-webapp-nginx/focal,focal 3.5.14+dfsg1.orig-1 all
    libnginx-mod-http-auth-pam/focal-updates,focal-security 1.18.0-0ubuntu1.2 amd64
    libnginx-mod-http-cache-purge/focal-updates,focal-security 1.18.0-0ubuntu1.2 amd64
    libnginx-mod-http-dav-ext/focal-updates,focal-security 1.18.0-0ubuntu1.2 amd64
    libnginx-mod-http-echo/focal-updates,focal-security 1.18.0-0ubuntu1.2 amd64
    libnginx-mod-http-fancyindex/focal-updates,focal-security 1.18.0-0ubuntu1.2 amd64
    libnginx-mod-http-geoip2/focal-updates,focal-security 1.18.0-0ubuntu1.2 amd64
    libnginx-mod-http-geoip/focal-updates,focal-security 1.18.0-0ubuntu1.2 amd64
    libnginx-mod-http-headers-more-filter/focal-updates,focal-security 1.18.0-0ubuntu1.2 amd64
    libnginx-mod-http-image-filter/focal-updates,focal-security 1.18.0-0ubuntu1.2 amd64
    ```
3.  * Add key of repo 
    ```
    vagrant@vagrant:~$ nano mysql_pubkey.asc
    ```
    * Insert key to file
    ```
    vagrant@vagrant:~$ gpg --import mysql_pubkey.asc
    gpg: directory '/home/vagrant/.gnupg' created
    gpg: keybox '/home/vagrant/.gnupg/pubring.kbx' created
    gpg: key 8C718D3B5072E1F5: 75 signatures not checked due to missing keys
    gpg: /home/vagrant/.gnupg/trustdb.gpg: trustdb created
    gpg: key 8C718D3B5072E1F5: public key "MySQL Release Engineering <mysql-build@oss.oracle.com>" imported
    gpg: Total number processed: 1
    gpg:               imported: 1
    gpg: no ultimately trusted keys found
    ```
    * Add repo to /etc/apt/sources.list
    ```
    deb http://repo.mysql.com/apt/{debian|ubuntu}/ {buster|bionic} {mysql-5.7|mysql-8.0|workbench-8.0|connector-python-8.0}
    ```
    ```
    sudo apt update
    sudo apt-get install mysql-server
    ```
    ```
    vagrant@vagrant:~$ sudo apt list | grep mysql | head

    WARNING: apt does not have a stable CLI interface. Use with caution in scripts.

    akonadi-backend-mysql/focal,focal 4:19.12.3-0ubuntu2 all
    asterisk-mysql/focal 1:16.2.1~dfsg-2ubuntu1 amd64
    automysqlbackup/focal,focal 2.6+debian.4-2 all
    bacula-common-mysql/focal 9.4.2-2ubuntu5 amd64
    bacula-director-mysql/focal,focal 9.4.2-2ubuntu5 all
    cl-sql-mysql/focal 6.7.0-1.1build2 amd64
    courier-authlib-mysql/focal 0.69.0-2build3 amd64
    cvm-mysql/focal 0.97-0.1build2 amd64
    dbconfig-mysql/focal,focal 2.0.13 all
    dbf2mysql/focal 1.14a-5.1build2 amd64
    ```
4.  ```
    vagrant@vagrant:~$ sudo systemctl enable nginx
    Synchronizing state of nginx.service with SysV service script with /lib/systemd/systemd-sysv-install.
    Executing: /lib/systemd/systemd-sysv-install enable nginx
    ```
    ```
    vagrant@vagrant:~$ sudo systemctl start nginx
    vagrant@vagrant:~$ sudo systemctl status nginx
    ● nginx.service - nginx - high performance web server
        Loaded: loaded (/lib/systemd/system/nginx.service; enabled; vendor preset: enabled)
        Active: active (running) since Sun 2021-12-12 22:43:14 UTC; 9s ago
        Docs: https://nginx.org/en/docs/
        Process: 16083 ExecStart=/usr/sbin/nginx -c /etc/nginx/nginx.conf (code=exited, status=0/SUCCESS)
    Main PID: 16094 (nginx)
        Tasks: 3 (limit: 1071)
        Memory: 4.0M
        CGroup: /system.slice/nginx.service
                ├─16094 nginx: master process /usr/sbin/nginx -c /etc/nginx/nginx.conf
                ├─16095 nginx: worker process
                └─16096 nginx: worker process

    Dec 12 22:43:14 vagrant systemd[1]: Starting nginx - high performance web server...
    Dec 12 22:43:14 vagrant systemd[1]: Started nginx - high performance web server.
    ```
    ```
    vagrant@vagrant:~$ sudo systemctl status mysql
    ● mysql.service - MySQL Community Server
        Loaded: loaded (/lib/systemd/system/mysql.service; enabled; vendor preset: enabled)
        Active: active (running) since Sun 2021-12-12 22:29:09 UTC; 15min ago
    Main PID: 15341 (mysqld)
        Status: "Server is operational"
        Tasks: 37 (limit: 1071)
        Memory: 352.7M
        CGroup: /system.slice/mysql.service
                └─15341 /usr/sbin/mysqld

    Dec 12 22:29:09 vagrant systemd[1]: Starting MySQL Community Server...
    Dec 12 22:29:09 vagrant systemd[1]: Started MySQL Community Server.
    ```
5.  ```
    vagrant@vagrant:~$ journalctl --unit=nginx
    -- Logs begin at Sun 2021-12-12 21:43:00 UTC, end at Sun 2021-12-12 22:47:06 UTC. --
    Dec 12 22:43:14 vagrant systemd[1]: Starting nginx - high performance web server...
    Dec 12 22:43:14 vagrant systemd[1]: Started nginx - high performance web server.
    Dec 12 22:46:11 vagrant systemd[1]: Stopping nginx - high performance web server...
    Dec 12 22:46:11 vagrant systemd[1]: nginx.service: Succeeded.
    Dec 12 22:46:11 vagrant systemd[1]: Stopped nginx - high performance web server.
    -- Reboot --
    Dec 12 22:46:22 vagrant systemd[1]: Starting nginx - high performance web server...
    Dec 12 22:46:22 vagrant systemd[1]: Started nginx - high performance web server.
    ```
    ```
    vagrant@vagrant:~$ journalctl --unit=mysql
    -- Logs begin at Sun 2021-12-12 21:43:00 UTC, end at Sun 2021-12-12 22:47:06 UTC. --
    Dec 12 22:29:09 vagrant systemd[1]: Starting MySQL Community Server...
    Dec 12 22:29:09 vagrant systemd[1]: Started MySQL Community Server.
    Dec 12 22:46:11 vagrant systemd[1]: Stopping MySQL Community Server...
    Dec 12 22:46:11 vagrant systemd[1]: mysql.service: Succeeded.
    Dec 12 22:46:11 vagrant systemd[1]: Stopped MySQL Community Server.
    -- Reboot --
    Dec 12 22:46:18 vagrant systemd[1]: Starting MySQL Community Server...
    Dec 12 22:46:19 vagrant systemd[1]: Started MySQL Community Server.
    ```


    
